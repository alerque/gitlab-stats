# Önceden Belirlemesi

## Tanrı’nın Bir Amacı Vardır

> > RAB, “Sizi sevdim” diyor. “Oysa siz, ‘Bizi nasıl sevdin?’ diye soruyorsunuz.” RAB, “Esav Yakup’un ağabeyi değil mi?” diye karşılık veriyor, “Ben Yakup’u sevdim, Esav’dan ise nefret ettim.”

###### Malaki 1:2–3

Kutsal Yazılar’ın altmış altı kitabını yaklaşık bin beş yüz yılda kaleme alan kırktan fazla yazar, kendilerini ve okuyucularını Tanrı’nın bu dünya için egemen amacının –yani O’nu yaratmaya yönlendiren sonra günahla bozulan ve şimdi kurtarma işiyle yeniden bina ettiği amacının– işleyişinin parçası olarak gördüler. Bu amaç özde, Tanrı ve akıl sahibi yaratıkları arasındaki sevgiden sonsuza dek zevk almak ve bunu ifade etmektir. Bu sevgi halkın Tanrı’ya sunduğu tapınmada, şükranda, hizmette, onurda, yücelikte, Tanrı’nın halkına sunduğu paydaşlıkta, ayrıcalıklarda, sevinçte ve armağanlarda görünür.

Yazarlar, günahtan zarar görmüş dünya gezegeni için Tanrı’nın kurtuluş planının halihazırda ne kadar ilerlediğini görmek için geriye bakarlar. Tanrı’nın planın tamamlanarak dünyanın hayal edilemez bir görkemle yeniden yaratılacağı günü görmek için ileriye bakarlar[^1]. Tanrı’nın Her Şeye Gücü Yeten Yaratıcı ve Kurtarıcı olduğunu ilan ederler ve Tanrı’nın sevgi vermek ve almakla ilgili özgün amacını yerine getirecek bir halk meydana getirmek için tarih boyunca lütfuyla gerçekleştirdiği çok yönlü işler üzerinde sürekli olarak dururlar. Yazarlar, Tanrı’nın planını o zaman için eriştiği noktaya getirirken mutlak bir kontrol sergilediği gibi, kendi isteği uyarınca her şeyi tamamen kontrol ederek çalışmayı sürdüreceğini ve kurtuluş projesini tamamlayacağını ısrarla vurgularlar. Önceden belirlenmeyle ilgili sorular bu referans çerçevesi dahilindedir[^2].

Önceden belirlenme kavramı, çoğunlukla Tanrı’nın dünya tarihinin akışı içinde geçmiş, şimdiki zaman ve gelecekteki bütün olayları önceden belirlemiş olmasını ifade eder ve bu kullanım oldukça yerindedir. Ancak Kutsal Yazılar’da ve ana akım teolojide önceden belirlenme, dünya ve içindekiler henüz var olmadan önce Tanrı’nın özellikle günahkârların kesin kaderiyle ilgili olarak verdiği karar anlamındadır. Esasen Yeni Antlaşma’da önceden belirlenme ve seçilme (ikisi birdir) ifadeleri yalnızca Tanrı’nın belirli günahkârları kurtuluş ve sonsuz yaşam için seçmesi için kullanılır[^3]. Ne var ki, birçokları Kutsal Yazılar’a göre Tanrı’nın en nihayetinde kurtulmayanlar hakkında da bir ön kararı olduğuna dikkat çekmiştir[^4], ve böylece Protestan teolojisinde Tanrı’nın önceden belirlemesi kavramını hem Tanrı’nın bazılarını günahtan kurtarma kararını (seçilmişlik) hem de geri kalanları günahları için mahkûm etme (ebedi mahvolmuşluk) kararını içerecek şekilde yan yana tanımlamak yaygın hale gelmiştir.

“Tanrı insanları hangi esasa göre seçti?” sorusuna kimi zaman verilen yanıt şudur: Müjde’yle yüzleştiklerinde Mesih’i Kurtarıcıları olarak seçeceklerine dair Tanrı’nın ön bilgisine göre. Bu yanıttaki ön bilgi, Tanrı’nın insanların eylemlerini önceden belirlemeksizin ne yapacakları hakkındaki pasif öngörüsü anlamındadır. Ancak (a) Romalılar 8:29; 11:2’deki *önceden bilmek* kavramı “önceden sevmek” ve “önceden atamak” anlamına gelir: bir gözlemcinin kendiliğinden neler olacağını beklemesi fikrini ifade etmez; (b) Herkes doğal olarak günahları içinde ölü olduğundan (yani, Tanrı’nın yaşamından uzaklaşmış ve O’na karşı duyarsız), Müjde’yi duyan hiç kimse ancak Tanrı’nın sağlayabileceği bir içsel canlandırma olmaksızın tövbeye ve imana gelemez[^5]. İsa, “Baba’nın bana yöneltmediği hiç kimse bana gelemez”[^6] dedi. Günahkârlar Mesih’i seçerler, çünkü Tanrı onları bu seçimi yapmaları için seçmiş ve yüreklerini yenileyerek onları buna yöneltmiştir.

Tüm insan eylemleri kendi başına belirleme bakımından özgür olsa da, hiçbiri Tanrı’nın ebedi amacı ve önceden belirlemesi ışığında O’nun kontrolünden bağımsız değildir.

Bu nedenle Hristiyanlar, imanları için Tanrı’ya şükretmeli, Tanrı’nın onları çağırdığı lütfun içinde tutması için O’na sığınmalı ve O’nun tasarısındaki gelecek kesin zaferini güven içinde beklemelidir.

[^1]: Yeşaya 65:17–25; 2. Petrus 3:10–13; Vahiy 21:1—22:5

[^2]: Efesliler 1:9–14; 2:4–10; 3:8–11; 4:11–16

[^3]: Romalılar 8:29; Efesliler 1:4–5,11

[^4]: Romalılar 9:6–29; 1. Petrus 2:8; Yahuda 4

[^5]: Efesliler 2:4–10

[^6]: Yuhanna 6:65, krş. 44; 10:25–28
