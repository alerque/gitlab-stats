.PHONY: all stats

all: book.pdf stats

%.pdf: %.md %.yml drawing.png
	m4 macros.m4 $< | pandoc - $*.yml -o $@

%.png: %.svg
	convert $< -resize 200x $@

stats:
	wc -l Makefile *.{md,svg,yml,m4}
